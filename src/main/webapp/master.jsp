<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" session="false"%><%
/*
    final String forwardUri = (String)request.getAttribute("javax.servlet.forward.request_uri");
    if(forwardUri != null && !forwardUri.isEmpty()) {
    	if(forwardUri.startsWith(request.getContextPath() + "/js/")) {
    		//request.setAttribute("javax.servlet.include.request_uri", forwardUri);
    		//request.setAttribute("javax.servlet.include.path_info", forwardUri);
    		//request.setAttribute("javax.servlet.include.servlet_path", "");

			System.out.println(forwardUri);

    		final HttpServletRequestWrapper forwardReq = new HttpServletRequestWrapper(request) {
    			public String getServletPath() {
    				return forwardUri;
    			}

    			public String getPathInfo() {
    				return "";
    			}
    		};

    		application.getNamedDispatcher("default").forward(forwardReq, response);
        	return;
    	}
    }
  */
    %><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>FIDELIO eService</title>
</head>
<body>
    Service deployed.<br/>
	<br/>
	<br/>
    <a href="http://127.0.0.1:24727/eID-Client?tcTokenURL=https://<%=request.getServerName()%>:<%= request.getServerPort() %><%= request.getContextPath() %>/?data=AaMBeCs2T0owTS1jR3JuazhaVkc1a09IdS0xYjU2MWQtUFhCa01Dc1FrZWQxZ3M4AlggF79NktcvevbtLIBY5hudyqEj44EXXH1oL_H_T1rDsEAF9g">
        Test FIDELIO eService - eID link.
    </a>
	<br/>

</body>
</html>
