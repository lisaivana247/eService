// ==UserScript==
// @name         Google Extension Proxy
// @namespace    http://vx4.de/
// @version      0.1
// @description  try to take over the world!
// @author       ck@vx4.de
// @match        https://*.google.com/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';
    console.log("Google FIDO hack active.");

    var osm = window.chrome.runtime.sendMessage, oc = window.chrome.runtime.connect,
    //    extId = 'klnjmillfildbbimkincljmfoepfhjjj'; // gnubbyd ext dev
    //extId = 'lkjlajklkdhaneeelolkfgbpikkgnkpk'; // gnubbyd ext stable
    //extId = 'dlfcjilkjfhdnfiecknlnddkmmiofjbg'; // gnubbyd app dev
    //extId = 'beknehfpfkghjoafdifaflglpjkojoco'; // gnubbyd app stable
    extId = 'kmendfapggjehodndflmmgagdbamhnfd'; // component
    //extId = "null";

    var port = {
        name: "",
        onDisconnect:  { listener: null, addListener: function(x) {
            port.onDisconnect.listener = x;
        }},
        onMessage:  { listener: null, addListener: function(x) {
            port.onMessage.listener = x; }, dispatch: function (x) { port.onMessage.listener(x); }
        },

        postMessage: function(msg, p) {
            console.log("postMessage: " + JSON.stringify(msg) + " / " + JSON.stringify(p));

            if(msg.type === "u2f_get_api_version_request") {
                console.log("!!!3");
                port.onMessage.dispatch({requestId: msg.requestId, responseData: { js_api_version: 1.1 }, type: 'u2f_get_api_version_response' });
            }

            if(msg.type === "u2f_register_request") {
                console.log("!!!4");
                window._FIDELIO.u2freg(msg.appId, msg.registerRequests, msg.registeredKeys, function(response) {
                    if(response.errorCode == 0) {
                        delete response.errorCode;
                    }
                    const res2 = { requestId: msg.requestId, type: 'u2f_register_response', responseData: response};
                    console.log("4 response: " + JSON.stringify(res2));
                    port.onMessage.dispatch(res2);
                }, msg.timeoutSeconds);
            }

            if(msg.type === "u2f_sign_request") {
                console.log("!!!5");
                window._FIDELIO.u2fsig(msg.appId, msg.challenge, msg.registeredKeys, function(response) {
                    if(response.errorCode == 0) {
                        delete response.errorCode;
                    }
                    console.log("5 response: " + JSON.stringify(response));
                    port.onMessage.dispatch({ requestId: msg.requestId, type: 'u2f_sign_response', responseData: response});
                }, msg.timeoutSeconds);
            }
        }
    };

    var nc = function(extensionId, connectObj) {
        console.log("                    connect to " + extensionId + " with " + JSON.stringify(connectObj));
        if(extensionId === extId) {
            console.log("!connect to " + extensionId + " with " + JSON.stringify(connectObj));
            console.trace();
            console.log("lastError: " + chrome.runtime.lastError);
            delete chrome.runtime.lastError;
            return port;
        } else return oc(extensionId, connectObj);
    };
    window.chrome.runtime.connect = nc;

    var nsm = function(extensionId, msg, cb) {
        console.log("                    sendMessage to " + extensionId + " with " + JSON.stringify(msg) + " / " + cb);
        if(extensionId === extId) {
            console.log("!sendMessage to " + extensionId + " with " + JSON.stringify(msg));
            console.trace();

            if(msg.type === "u2f_get_api_version_request") {
                console.log("!!!1");
                cb({ requestId: msg.requestId, responseData: { js_api_version: 1.1 }, type: 'u2f_get_api_version_response' });
            }

            if(msg.type === "u2f_sign_request") { //{"type":"u2f_sign_request","signRequests":[]}
                console.log("!!!2");
                cb({type: 'u2f_sign_response', responseData: {errorCode: 2}});
            }

            console.log("lastError: " + chrome.runtime.lastError);
            delete chrome.runtime.lastError;
        } else osm(extensionId, msg, cb);
    };
    window.chrome.runtime.sendMessage = nsm;
})();
