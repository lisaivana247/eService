<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" session="false" import="java.io.*, java.net.*, java.util.*"%><%!
%><%!
	private final Map<String, String> map = new HashMap<String, String>();
	
	private String xml(final String src, final String tag) {
		int n = src != null ? src.indexOf("<" + tag + ">") : -1;
		if(tag != null && n >= 0 && src.length() > (n = n + tag.length() + 2)) {
			return src.substring(n, src.indexOf("</" + tag + ">", n));
		}
		return null;
	}

	// TODO: validate with HMAC(trust-key, ...) before use
	private String validate(final String id) {
	    return id;
	}
%><%
    final String hostHeader = request.getHeader("host"), query = request.getQueryString();
  	String id = null;
    try {
        if(hostHeader == null || hostHeader.isEmpty() || query == null || query.length() < 3) {
            response.setHeader("WWW-Authenticate", "Basic realm=\"" + "FIDELIO\"");
            response.setStatus(401);
        } else if("env".equals(query)) {
            out.write(request.getRequestURL().toString());
        } else if((id = validate(request.getParameter("join"))) != null) { // XXX: validate origin before use
            System.out.println("joiner id = " + id);
            map.put(id, map.remove(id + "_")); // move URL to "live" state, so it can be delivered
        } else if((id = validate(request.getParameter("wait"))) != null) { // XXX: validate origin before use
            System.out.println("wait call: " + id + " = " + map.get(id + "_") + " => " + map.get(id));
            while(map.get(id) == null) {
                try {
                    Thread.sleep(250);
                } catch(InterruptedException ie) {
                }
            }
            System.out.println("wait call solved: " + id + " = " + map.get(id));

            response.setStatus(303);
            response.setHeader("Location", map.remove(id) + "&ResultMajor=ok"); // XXX: copy from real query
        } else if((id = validate(request.getParameter("id"))) != null) { // XXX: validate origin before use
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            final HttpServletResponse res2 = new HttpServletResponseWrapper(response) {
                private PrintWriter pw = new PrintWriter(baos);
                public PrintWriter getWriter() {
                    return pw;
                }
                public ServletOutputStream getOutputStream() {
                    return null;
                }
            };

            request.getRequestDispatcher("/x").forward(request, res2);
            res2.getWriter().close();

            final String body = new String(baos.toByteArray()), refreshUrl = xml(body, "RefreshAddress"),
                baseURL = "https://" + request.getServerName() +
                (request.getServerPort() == 443 ? "" : ":" + request.getServerPort()) + request.getContextPath();

            System.out.println("body: " + body);
            System.out.println("mapping " + id + "_" + " to " + refreshUrl);
            map.put(id + "_", refreshUrl);
            response.getWriter().println(body.replace(refreshUrl, baseURL + "/$2/?join=" + id));
        } else {
            request.getRequestDispatcher("/x").forward(request, response);
        }
    } catch(final Exception e) {
        e.printStackTrace();
        %>Error initializing service / setup incomplete.<%
    }
%>