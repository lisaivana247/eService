/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.nstant.in.cbor.CborBuilder;
import co.nstant.in.cbor.CborDecoder;
import co.nstant.in.cbor.CborEncoder;
import de.bund.bsi.fidelio.poc.core.format.FIDELIORequest;
import de.bund.bsi.fidelio.poc.core.format.FIDELIOResponse;
import de.bund.bsi.fidelio.poc.service.TokenKeyService.Context;
import de.persoapp.core.util.Hex;


/**
 * Frontend servlet implementation class MainServlet
 *
 * @author Christian Kahlo, 2017
 * @version $Id$
 */

// candidate for removal
// @WebServlet(description = "main entry point for requests", urlPatterns = { "/" })

public class MainServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private TokenKeyService tks;
	private FIDELIO fidelio;

	private final Map<String, String> knownAppIds = new HashMap<String, String>();


	/**
	 * Constructor for this servlet.
	 *
	 * @see HttpServlet#HttpServlet()
	 */
	public MainServlet() {
		super();
	}


	/**
	 * Initializes this servlet instance based on provided servlet configuration. It searches for a config property file
	 * and initializes the token key service and FIDELIO core service instances.
	 *
	 * @see javax.servlet.GenericServlet#init(javax.servlet.ServletConfig)
	 */
	@Override
	public void init(final ServletConfig config) throws ServletException {
		super.init(config);

		final Properties configProps = (Properties) config.getServletContext()
				.getAttribute(ContextListener.CONFIG_PROPS_ATTRIBUTE);

		if (configProps == null) {
			throw new ServletException("No configuration.");
		}

		final String srvRoot = new File(config.getServletContext().getRealPath("/"), "../../").getAbsolutePath();

		final Properties eIDProps = new Properties();
		final Properties fidoProps = new Properties();

		// filter properties by prefix
		// this can also be done using streams but would be horribly readable

		for (final Object key : configProps.keySet()) {
			final String propName = (String) key;
			final String value = configProps.getProperty(propName).replace("${confRoot}", srvRoot);

			if (propName != null && propName.startsWith("eid.")) {
				eIDProps.setProperty(propName.substring(4), value);
			} else if (propName != null && propName.startsWith("fidelio.")) {
				fidoProps.setProperty(propName.substring(8), value);
			} else {
				this.log("Unknown property: " + propName);
			}
		}

		for (final Object key : fidoProps.keySet()) {
			final String propName = (String) key;
			if (propName != null && propName.startsWith("appId.")) {
				this.knownAppIds.put(propName.substring(6), fidoProps.getProperty(propName));
			}
		}

		try {
			this.tks = new TokenKeyServiceImpl(eIDProps);
		} catch (final GeneralSecurityException e) {
			throw new ServletException(e);
		}

		this.fidelio = new FIDELIO(fidoProps);
	}


	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void service(final HttpServletRequest request, final HttpServletResponse response)
			throws ServletException, IOException {

		// enforce https:// protocol, use configured name and port of server
		final String baseURL = "https://" + request.getServerName()
				+ (request.getServerPort() == 443 ? "" : ":" + request.getServerPort())
				+ request.getContextPath();

		request.setAttribute("baseURL", baseURL);
		response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload");
		response.setHeader("Access-Control-Allow-Origin", "*");

		if (!"GET".equalsIgnoreCase(request.getMethod())) {
			response.sendError(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
			return;
		}

		// avoid HTTP sessions
		final HttpSession oldSession = request.getSession(false);
		if (oldSession != null) {
			oldSession.invalidate();
		}

		// delete all wrong or accidentally set cookies
		final Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (final Cookie cookie : cookies) {
				cookie.setValue("");
				cookie.setPath("/");
				cookie.setMaxAge(0);
				response.addCookie(cookie);
			}
		}

		final String qs = request.getQueryString();
		if (qs == null || qs.isEmpty()) {
			request.getRequestDispatcher("/master.jsp").forward(request, response);
			return;
		}

		// TBD: TLS check?
		// TBD: Client check?
		// TBD: request integrity check

		final String pData = request.getParameter("data");
		if (pData != null && !pData.trim().isEmpty()) {
			try {
				final ByteArrayInputStream bais = new ByteArrayInputStream(Base64.getUrlDecoder().decode(pData));
				final byte cmd = (byte) (bais.read() & 0xFF);
				if (cmd >= 0x01 && cmd <= 0x04) {
					final Object cborData = CBORUtil.from(new CborDecoder(bais).decodeNext());
					if (cborData != null && cborData instanceof Map) {
						final FIDELIORequest fidoReq = new FIDELIORequest(cmd, (Map<?, ?>) cborData);

						final byte[] appIdHash = fidoReq.getAppIdHash();
						if (appIdHash != null && appIdHash.length == 32) {
							final String appId = Hex.x(appIdHash);
							final String realm = "FIDELIO: " +
							// decode appIdHash and map to service for display, 1234...9876
									(this.knownAppIds.containsKey(appId) ? this.knownAppIds.get(appId)
											: appId.substring(0, 2) + "..." + appId.substring(appId.length() - 2));

							final Context tksCtx = this.tks.create(realm);
							if (tksCtx != null) {
								tksCtx.put("RemoteHost", request.getRemoteHost());
								tksCtx.put("callback", request.getParameter("callback"));
								tksCtx.put(FIDELIORequest.class.getName(), fidoReq);
								tksCtx.connect(appIdHash, response, baseURL + "/?s=");
							}
						} else {
							response.sendError(503, "CTAP request invalid.");
						}
					} else {
						response.sendError(503,
								"Received unknown value: " + (cborData == null ? "<null>" : cborData.toString()));
					}
				} else {
					response.sendError(503, "Invalid FIDO request command.");
				}
			} catch (final Exception e) {
				Throwable cause = e;
				while (cause.getCause() != null) {
					cause = cause.getCause();
				}

				e.printStackTrace();
				response.sendError(503,
						"Error creating session: " + e.getMessage() + " / " + cause.getClass().getName());
				return;
			}
		} else if ("ok".equalsIgnoreCase(request.getParameter("ResultMajor")) && request.getParameter("s") != null) {
			final Context tksCtx = this.tks.finish(request.getParameter("s"));
			if (tksCtx == null || !request.getRemoteHost().equals(tksCtx.get("RemoteHost"))) {
				throw new RuntimeException("Security Error");
			}

			final FIDELIORequest fidoReq = (FIDELIORequest) tksCtx.get(FIDELIORequest.class.getName());
			if (fidoReq != null && fidoReq.getAppIdHash() != null && fidoReq.getClientDataHash() != null) {
				try {
					final FIDELIOResponse fidoRes = this.fidelio.doFIDOeID(tksCtx, fidoReq);
					final ByteArrayOutputStream baos = new ByteArrayOutputStream();
					baos.write(fidoRes.getStatus());

					new CborEncoder(baos).encode(new CborBuilder().addMap()
							.put(1, fidoRes.getKeyHandle()).put(3, fidoRes.getResponse()).end().build());

					final byte[] ctapMsg = baos.toByteArray();

					response.setStatus(200);
					final String callback = (String)tksCtx.get("callback");
					if(callback != null && !callback.isEmpty()) {
					    final byte[] jsonp = (callback + "(\"" + Base64.getUrlEncoder().encodeToString(ctapMsg) + "\");").getBytes();
                        response.setContentType("application/javascript");
                        response.setContentLength(jsonp.length);
                        response.getOutputStream().write(jsonp);
                    } else {
						response.setCharacterEncoding(null);
                        response.setContentType("application/cbor");
                        response.setContentLength(ctapMsg.length);
                        response.getOutputStream().write(ctapMsg);
                    }
                    response.getOutputStream().close();
				} catch (final Exception e) {
					e.printStackTrace();
					response.sendError(500, "FIDELIO Server error: " + e.getMessage());
				}
			} else {
				response.sendError(500, "FIDO request invalid.");
				// + ctx.result.getResultCode());
			}
		} else {
			response.sendError(404, "Missing FIDO request / failed authentication.");
		}
	}
}
