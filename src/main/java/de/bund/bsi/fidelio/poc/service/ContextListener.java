/*
 * Copyright 2016-2017 adesso AG
 *
 * Licensed under the EUPL, Version 1.1 or - as soon they will be approved by the
 * European Commission - subsequent versions of the EUPL (the "Licence"); You may
 * not use this work except in compliance with the Licence.
 *
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to in writing, software distributed
 * under the Licence is distributed on an "AS IS" basis, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied. See the Licence for the
 * specific language governing permissions and limitations under the Licence.
 */

package de.bund.bsi.fidelio.poc.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * Context listener to find and load initial configuration file. Uses the servlet parameter <b>configFile</b> to
 * override default value ("configFile"). Places an attribute <b>configProps</b> in the servlet context.
 *
 * @author Christian Kahlo, 2015
 * @version $Id$
 */

public class ContextListener implements ServletContextListener {

	public static final String CONFIG_PROPS_ATTRIBUTE = "configProps";


	@Override
	public void contextInitialized(final ServletContextEvent sce) {
		final ServletContext sc = sce.getServletContext();
		if (sc != null) {

			final String configFileParam = sc.getInitParameter("configFilePrefix") + "-" + sc.getContextPath()
					.replace("/", "") + ".conf";

 			try {
				sc.log("configured config file: " + new File(configFileParam).getCanonicalPath());
			} catch (final IOException e1) {
				sc.log("invalid config file: " + configFileParam + " / " + e1.getMessage());
			}

			if (configFileParam != null) {
				final File configFile = new File(sc.getRealPath("/"), configFileParam);
				if (!configFile.exists() || !configFile.isFile() || !configFile.canRead()) {
					sc.log("invalid path to configfile: " + configFile.toString());
				} else {
					try {
						sc.log("using config file: " + configFile.getCanonicalPath());

						final Properties configProps = new Properties();
						try (FileInputStream fis = new FileInputStream(configFile)) {
							configProps.load(fis);
						}

						sc.setAttribute(CONFIG_PROPS_ATTRIBUTE, configProps);
					} catch (final IOException e) {
						e.printStackTrace();
					}

				}
			}
		}
	}


	@Override
	public void contextDestroyed(final ServletContextEvent sce) {
		sce.getServletContext().removeAttribute(CONFIG_PROPS_ATTRIBUTE);
	}

}
